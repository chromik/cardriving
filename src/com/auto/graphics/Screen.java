package com.auto.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import com.auto.car.CarDemo;
import com.auto.car.level.tile.Tile;

/*
 * @Description: This class takes care of all graphic rendering on the screen.   
 */
public class Screen {
	private int width;
	private int height;
	private int[] pixels;
	private int xOffset, yOffset;
	private Graphics2D graphics;
	private int scale = CarDemo.scale;
	
	public Screen() {

	}

	public Screen(int w, int h) {
		width = w;
		height = h;
		pixels = new int[w * h];
	}

	public int[] getPixels() {
		return pixels;
	}

	/*
	 * @Description: This function renders single Tile on screen
	 * 
	 * @param xPosition x coordinate of Tile on screen.
	 * 
	 * @param yPosition y coordinate of Tile on screen.
	 * 
	 * @param tile the tile to be rendered.
	 */
	public void renderTile(int xPosition, int yPosition, Tile tile) {
		// substract the tile position by the offset of screen when screen move
		xPosition -= xOffset;
		yPosition -= yOffset;
		for (int yTile = 0; yTile < tile.size; yTile++) {
			int yScreen = yTile + yPosition;
			for (int xTile = 0; xTile < tile.size; xTile++) {
				int xScreen = xTile + xPosition;
				// if the xScreen and yScreen are out of boundary then no
				// rendering. -tile.size is to render the boundary of the map
				// without rendering black strike.
				if (xScreen < -tile.size || xScreen >= width || yScreen < 0
						|| yScreen >= height) {
					break;
				}
				if (xScreen < 0) {
					xScreen = 0;
				}
				pixels[xScreen + yScreen * width] = tile.sprite.getPixels()[xTile
						+ yTile * tile.size];
			}
		}
	}

	/*
	 * @Description: This function renders single Sprite on screen
	 * 
	 * @param xPosition x coordinate of Sprite on screen.
	 * 
	 * @param yPosition y coordinate of Sprite on screen.
	 * 
	 * @param angle The angle of the car.
	 * 
	 * @param sprite the sprite to be rendered.
	 */
	public void renderCar(int xPosition, int yPosition, double angle,
			Sprite sprite) {
		// substract the Tile position by the offset of screen when screen moves
		xPosition -= xOffset;
		yPosition -= yOffset;
		BufferedImage img = new BufferedImage(Sprite.SIZE, Sprite.SIZE,
				BufferedImage.TYPE_INT_ARGB);
		for (int ySprite = 0; ySprite < Sprite.SIZE; ySprite++) {
			for (int xSprite = 0; xSprite < Sprite.SIZE; xSprite++) {
				int color = sprite.getPixels()[(xSprite + ySprite * Sprite.SIZE)];
				if (color != 0xffffffff) {
					img.setRGB(xSprite, ySprite, color);
				}
			}
		}
		AffineTransform reset = new AffineTransform();
		reset.rotate(0, 0, 0);
		graphics.rotate(angle, (xPosition + Sprite.SIZE / 2) * scale,
				(yPosition + Sprite.SIZE / 2) * scale);
		graphics.drawImage(img, xPosition * scale, yPosition * scale,
				Sprite.SIZE * scale, Sprite.SIZE * scale, null);
		/*
		 * graphics.drawRect(xPosition * scale, yPosition * scale, Sprite.SIZE
		 * scale, Sprite.SIZE * scale);
		 */
		graphics.setTransform(reset);
	}

	/*
	 * @Description: draw a line between 2 points
	 * 
	 * @param x xCoordinate of first point
	 * 
	 * @param y yCoordinate of first point
	 * 
	 * @param x2 xCoordinate of second point
	 * 
	 * @param y2 yCoordinate of second point
	 * 
	 * @param color the color of line
	 */
	public void renderLine(double x, double y, double x2, double y2, Color color) {
		graphics.setColor(color);
		graphics.drawLine(((int) (x - xOffset + Sprite.SIZE / 2)) * scale,
				((int) (y - yOffset + Sprite.SIZE / 2)) * scale, ((int) (x2
						- xOffset + Sprite.SIZE / 2))
						* scale, ((int) (y2 - yOffset + Sprite.SIZE / 2))
						* scale);

	}

	/*
	 * @Description: render a circle from a certain point
	 * 
	 * @param x xCoordinate of center of circle
	 * 
	 * @param y yCoordinate of center of cicle
	 * 
	 * @param r radius of circle
	 * 
	 * @param color the color of circle
	 */
	public void renderCircle(double x, double y, double r, Color color) {
		graphics.setColor(color);
		graphics.drawOval((int) (x - xOffset - r + Sprite.SIZE / 2) * scale,
				(int) (y - r - yOffset + Sprite.SIZE / 2) * scale,
				(int) (2 * r * scale), (int) (2 * r * scale));
	}

	public void setGraphic(Graphics2D g) {
		this.graphics = g;
	}

	public void renderStatistics(ArrayList<String> info) {
		graphics.setColor(Color.black);
		graphics.setFont(graphics.getFont().deriveFont(20f));
		graphics.drawString(info.get(0), 700, 20);
		graphics.drawString(info.get(1), 700, 40);
		graphics.drawString(info.get(2), 700, 60);
		graphics.drawString(info.get(3), 700, 80);
	}

	public void dispose() {
		graphics.dispose();
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setOffset(int xOffset, int yOffset) {
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}

	public int getXOffset() {
		return xOffset;
	}

	public int getYOffset() {
		return yOffset;
	}

}
