package com.auto.algorithm;

import java.util.ArrayList;

/*
 * @Description: NeuralNetwork is used to make decision for the car; decide that it
 * should turn right or turn left or go straight. It may contain many hidden NeuronsLayers
 * and 1 output NeuronsLayer. These layers will constantly update to get new values from genomes
 * each time the car crashes to the wall. The network will use new values from Genetic to make
 * decision for the next try of car.
 */
public class NeuralNetwork {
	private int outputAmount;
	private ArrayList<Double> outputs;
	private ArrayList<Double> inputs;
	private ArrayList<NeuronsLayer> hiddenLayers;
	private NeuronsLayer outputLayer;

	public NeuralNetwork() {
		outputs = new ArrayList<>();
		inputs = new ArrayList<>();
	}

	public void setInput(double[] normalizedDepths) {
		inputs.clear();
		for (int i = 0; i < normalizedDepths.length; ++i) {
			inputs.add(normalizedDepths[i]);
		}
	}

	@SuppressWarnings("unchecked")
	public void update() {
		outputs.clear();
		for (int i = 0; i < hiddenLayers.size(); ++i) {
			if (i > 0) {
				inputs = outputs;
			}
			// each hidden layer calculates the outputs based on inputs from sensors of the car
			hiddenLayers.get(i).evaluate(inputs, outputs);
			// TODO: UNCOMMENT: System.out.println("Output of hiddden layers: " + outputs.toString());
		}
		// the outputs of HiddenLayers will be used as input for OutputLayer
		inputs = (ArrayList<Double>) outputs.clone();

		// The output layer will give out the final outputs
		outputLayer.evaluate(inputs, outputs);
	}

	public double getOutput(int index) {
		if (index >= outputAmount)
			return 0.0f;
		return outputs.get(index);
	}

	/*
	 * Initiate NeuronsNetwork
	 */
	/*
	 * public void createNet(int numOfHiddenLayers, int numOfInputs, int
	 * neuronsPerHidden, int numOfOutputs) { inputAmount = numOfInputs;
	 * outputAmount = numOfOutputs; hiddenLayers = new
	 * ArrayList<NeuronsLayer>(); for (int i = 0; i < numOfHiddenLayers; i++) {
	 * NeuronsLayer layer = new NeuronsLayer();
	 * layer.populateLayer(neuronsPerHidden, numOfInputs);
	 * hiddenLayers.add(layer); } outputLayer = new NeuronsLayer();
	 * outputLayer.populateLayer(numOfOutputs, neuronsPerHidden); }
	 */
	public void releaseNet() {
		outputLayer = null;
		hiddenLayers = null;
	}

    /*
     * Neural network receives weights from genome to make new HiddenLayers and
     * OutputLayer.
     */
	public void fromGenome(Genome genome, int neuronsPerHidden, int numOfOutputs) {

		if (genome != null) {
            releaseNet();
            hiddenLayers = new ArrayList<>();
            outputAmount = numOfOutputs;

            ArrayList<Neuron> hiddenLayer = createLayerWithGenome(genome, neuronsPerHidden, neuronsPerHidden);
            NeuronsLayer hidden = new NeuronsLayer();
            hidden.loadLayer(hiddenLayer);

            hiddenLayers.add(hidden);

            ArrayList<Neuron> outputLayer = createLayerWithGenome(genome, numOfOutputs, neuronsPerHidden);
            this.outputLayer = new NeuronsLayer();
            this.outputLayer.loadLayer(outputLayer);

        }
	}

	private ArrayList<Neuron> createLayerWithGenome(Genome genome, int numOfNeurons, int inputsNumForSingleNeuron) {
        ArrayList<Neuron> neurons = new ArrayList<>();
        for (int i = 0; i < numOfNeurons; ++i) {
            ArrayList<Double> weights = new ArrayList<>();
            for (int j = 0; j < inputsNumForSingleNeuron + 1; ++j) {
                weights.add(genome.weights.get(i * inputsNumForSingleNeuron + j));
            }
            Neuron n = new Neuron();
            n.init(weights, inputsNumForSingleNeuron);
            neurons.add(n);
        }
        return neurons;
    }

}
