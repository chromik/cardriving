package com.auto.car.entity;

import com.auto.car.level.Level;
import com.auto.graphics.Screen;

public abstract class Entity {
	protected int x;
	protected int y;
	protected Level level;

	public abstract void update();

	public abstract void render(Screen screen);

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}
}
